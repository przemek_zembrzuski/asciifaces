## AsciiFaces

AsciiFaces allows you to copy to the clipboard an ascii face.

## Installation & running

git clone https://przemek_zembrzuski@bitbucket.org/przemek_zembrzuski/asciifaces.git asciifaces  
cd asciifaces  
npm install  
npm start
Install expo app from the Google play store (android)
Scan QR code which one was printed in the terminal after compilation

## License

AsciiFaces is ISC licensed