import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Header from './Components/Header'
import Content from './Components/Content'

export default class App extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Header />
        <Content />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
