import React from 'react';
import { StyleSheet, Text, View, FlatList, TouchableHighlight, Clipboard, Dimensions } from 'react-native';

import {asciiFaces} from '../asciiFaces';

const width = Dimensions.get('window').width;

export default class Content extends React.Component {
  onPress(){
    Clipboard.setString(this.children.props.children);
  }
  render() {
    return (
      <View style={styles.container}>
        <FlatList
            data = {asciiFaces}
            renderItem = {items => <View style={styles.itemContainer}><TouchableHighlight underlayColor={'transparent'} onPress={this.onPress}><Text style={styles.item}>{items.item}</Text></TouchableHighlight></View>}
            keyExtractor = {(items,index) => index}
        />  
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#44E5E7',
    alignItems: 'center',
    justifyContent: 'center'
  },
  itemContainer: {
    width:width,
    marginTop: 10,
    marginBottom: 10,
    alignItems: 'center',
    justifyContent: 'center'
  },
  item:{
      width:200,
      height: 40,
      borderRadius:5,
      paddingTop:10,
      textAlign:'center',
      color:'#333333',
      backgroundColor:'#ffffff'
  }
});