import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

export default class Header extends React.Component {
  render() {
    return (
      <View style={styles.header}>
        <Text style={styles.text}>FacesApp</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    flex: 1,
    maxHeight:60,
    backgroundColor: '#59D2FE',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    color:'#ffffff',
    fontSize:30
  }
});
